import Mock from 'mockjs'

const List = []
const count = 200 * Math.random()

// const baseContent = '<p>I am testing data, I am testing data.</p><p><img src="https://wpimg.wallstcn.com/4c69009c-0fd4-4153-b112-6cb53d1cf943"></p>'
// const image_uri = 'https://wpimg.wallstcn.com/e4558086-631c-425c-9430-56ffb46e70b3'

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    username: '@first',
    operation: Mock.Random.word(3, 15),
    fun: Mock.Random.word(3, 15),
    param: Mock.Random.word(3, 15),
    requeTime: +Mock.Random.date('T'),
    ipAddress: Mock.Random.ip(),
    status: ['1', '2', '3'],
    createTime: +Mock.Random.date('T')
  }))
}

export default [
  {
    url: '/journal/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = config.query.pageNum, limit = config.query.pageSize, sort } = config.query
      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
      return {
        code: 20000,
        data: {
          total: mockList.length,
          page: ~~config.query.pageNum,
          items: pageList
        }
      }
    }
  },

  {
    url: '/journal/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const journal of List) {
        if (journal.id === +id) {
          return {
            code: 200,
            data: journal
          }
        }
      }
    }
  },

  {
    url: '/journal/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/journal/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/journal/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

