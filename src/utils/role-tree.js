/**
 * 数组扁平化
 * @param {Array} arr
 */
export function flatten(arr) {
  var res = []
  for (var i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i].children) && arr[i].children.length > 0) {
      res.push(arr[i])
      res = res.concat(flatten(arr[i].children))
    } else {
      res.push(arr[i])
    }
  }
  return res
}

/**
 * 数组去重
 * @param {Array} arr
 */
export function unique(arr) {
  var x = new Set(arr)
  return [...x]
}

export function noRepeat(arr, value) {
  // id去重
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === value) {
      return false
    }
  }
  return true
}

export function setHalfParent(value, data, arr) {
  // 通过查询父节点下是否右子节点 将半选状态的父节点加入数组中
  for (var i = 0; i < data.length; i++) {
    var mod = data[i]
    if (mod.menuId === value) {
      if (noRepeat(arr, mod.menuId)) {
        arr.push(mod.menuId)
      }
      return true
    } else {
      if (mod.children && mod.children.length > 0) {
        var bool = setHalfParent(value, mod.children, arr)
        if (bool) {
          if (noRepeat(arr, mod.menuId)) {
            arr.push(mod.menuId)
          }
          return true
        }
      }
    }
  }
  return false
}

export function getHalfParent(value, data, arr) {
  for (var i = 0; i < data.length; i++) {
    var mod = data[i]
    if (mod.menuId === value) {
      if (mod.children.length === 0) {
        arr.push(mod.menuId)
        return true
      }
      return true
    } else {
      if (mod.children.length !== 0) {
        getHalfParent(value, mod.children, arr)
      }
    }
  }
  return true
}
