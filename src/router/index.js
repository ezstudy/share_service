import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
/*
  路由切换
  人事管理 personManagement
  工作流 works
  信息管理 webManagement
  系统设置 setAdmin
  type: 'xxxx',

*/
/* Layout */
import Layout from '@/layout'

/* Router Modules */
// import componentsRouter from './modules/components'
// import chartsRouter from './modules/charts'
// import tableRouter from './modules/table'
// import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'home',
    hidden: true,
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/index'),
        name: 'home',
        meta: { title: '主页', icon: 'dashboard', noCache: true, affix: true }
      }
    ]
  }

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/activityModel',
    roleName: 'activityManage',
    type: 'play',
    component: Layout,
    redirect: 'activityModel/activityManage',
    meta: { title: '运营管理', icon: 'star', noCache: true },
    hidden: true,
    // always: true,
    children: [
      {
        path: 'activityManage',
        component: () => import('@/views/activityManage/index'),
        name: 'activityManage',
        meta: { title: '运营活动管理', noCache: true }
      }
    ]
  },
  {
    path: '/coursesModel',
    roleName: 'coursesManage',
    type: 'play',
    component: Layout,
    redirect: 'coursesModel/coursesManage',
    meta: { title: '课程管理', icon: 'form', noCache: true },
    hidden: true,
    // always: true,
    children: [
      {
        path: 'coursesManage',
        component: () => import('@/views/coursesManage/index'),
        name: 'coursesManage',
        meta: { title: '课程管理', noCache: true }
      }
    ]
  },
  {
    path: '/liveModel',
    roleName: 'liveManage',
    type: 'play',
    component: Layout,
    redirect: 'liveModel/liveManage',
    meta: { title: '直播管理', icon: 'live', noCache: true },
    hidden: true,
    always: true,
    children: [
      {
        path: 'liveManage',
        component: () => import('@/views/liveManage/index'),
        name: 'liveManage',
        meta: { title: '直播管理', noCache: true }
      }
    ]
  },
  {
    path: '/livePreview',
    // roleName: 'organization',
    component: Layout,
    redirect: 'livePreview/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '直播预览', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/livePreview/index'),
      name: 'livePreview',
      meta: { title: '直播预览', noCache: false }
    }]
  },
  {
    path: '/liveDetails',
    // roleName: 'organization',
    component: Layout,
    redirect: 'liveDetails/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '直播详情', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/liveDetails/index'),
      name: 'liveDetails',
      meta: { title: '直播详情', noCache: false }
    }]
  },
  {
    path: '/livePlayback',
    // roleName: 'organization',
    component: Layout,
    redirect: 'livePlayback/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '直播回放', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/livePlayback/index'),
      name: 'livePlayback',
      meta: { title: '直播回放', noCache: false }
    }]
  },
  {
    path: '/liveStatistics',
    // roleName: 'organization',
    component: Layout,
    redirect: 'liveStatistics/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '直播统计', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/liveStatistics/index'),
      name: 'liveStatistics',
      meta: { title: '直播统计', noCache: false }
    }]
  },
  {
    path: '/recordModel',
    roleName: 'recordManage',
    type: 'play',
    component: Layout,
    redirect: 'recordModel/recordManage',
    meta: { title: '录播管理', icon: 'table', noCache: true },
    hidden: true,
    // always: true,
    children: [
      {
        path: 'recordManage',
        component: () => import('@/views/recordManage/index'),
        name: 'recordManage',
        roleName: 'recordList',
        meta: { title: '录播管理', keepAlive: false }
      },
      {
        path: 'categoryManage',
        component: () => import('@/views/categoryManage/index'),
        name: 'categoryManage',
        roleName: 'categoryManage',
        meta: { title: '分类管理', keepAlive: false }
      }
    ]
  },
  {
    path: '/activityDetails',
    // roleName: 'organization',
    component: Layout,
    redirect: 'activityDetails/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '活动详情', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/activityDetails/index'),
      name: 'activityDetails',
      meta: { title: '活动详情', noCache: false }
    }]
  },
  {
    path: '/coursesStages',
    // roleName: 'organization',
    component: Layout,
    redirect: 'coursesStages/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '课程阶段', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/coursesStages/index'),
      name: 'coursesStages',
      meta: { title: '课程阶段', noCache: false }
    }]
  },
  {
    path: '/coursesItems',
    // roleName: 'organization',
    component: Layout,
    redirect: 'coursesItems/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '课程课次', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/coursesItems/index'),
      name: 'coursesItems',
      meta: { title: '课程课次', noCache: false }
    }]
  },
  {
    path: '/voteResult',
    // roleName: 'organization',
    component: Layout,
    redirect: 'voteResult/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '投票结果', icon: 'chart', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/voteResult/index'),
      name: 'voteResult',
      meta: { title: '投票结果', noCache: false }
    }]
  },
  {
    path: '/uploadVideo',
    // roleName: 'organization',
    component: Layout,
    redirect: 'uploadVideo/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '上传视频', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/uploadVideo/index'),
      name: 'uploadVideo',
      meta: { title: '上传视频', noCache: false, keepAlive: true }
    }]
  },
  {
    path: '/editVideo',
    // roleName: 'organization',
    component: Layout,
    redirect: 'editVideo/index', // 重定向地址，在面包屑中点击会重定向去的地址
    hidden: true,
    always: true,
    meta: { title: '编辑视频', icon: 'edit', noCache: true },
    children: [{
      path: 'index',
      component: () => import('@/views/editVideoMsg/index'),
      name: 'editVideo',
      meta: { title: '编辑视频', noCache: true }
    }]
  },
  {
    path: '/paymagement',
    roleName: 'paymanage',
    redirect: '/paymagement/list',
    component: Layout,
    type: 'play',
    hidden: true,
    meta: {
      title: '支付详情',
      icon: 'nested'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/paymagement'),
        name: 'paymagementList',
        roleName: 'paylist',
        meta: {
          title: '支付详情'
        }
      },
      {
        path: '',
        component: () => import('@/views/pay-manage'),
        name: 'paymanage',
        roleName: 'payconfig',
        meta: {
          title: '支付配置'
        }
      }
    ]
  },
  // {
  //   path: '/excelImport',
  //   roleName: 'excelImport',
  //   component: Layout,
  //   type: 'setAdmin',
  //   hidden: true,
  //   children: [
  //     {
  //       path: '',
  //       component: () => import('@/views/excel-import'),
  //       name: 'excelImport',
  //       meta: {
  //         title: '批量导入测试',
  //         icon: 'excel'
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/postManage',
  //   roleName: 'postManage',
  //   redirect: '/postManage/list',
  //   component: Layout,
  //   hidden: true,
  //   type: 'setAdmin',
  //   meta: {
  //     title: '岗位管理',
  //     icon: 'tab'
  //   },
  //   children: [
  //     {
  //       path: 'list',
  //       component: () => import('@/views/post-manage'),
  //       name: 'postManage',
  //       meta: {
  //         title: '岗位管理',
  //         icon: 'tab'
  //       }
  //     }
  //   ]
  // },
  {
    path: '/organization',
    roleName: 'organization',
    redirect: '/organization/list',
    component: Layout,
    type: 'setAdmin',
    hidden: true,
    meta: {
      title: '组织架构',
      icon: 'list'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/organization'),
        name: 'organizationList',
        meta: {
          title: '组织架构'
        }
      }
    ]
  },
  {
    path: '/postManage',
    name: 'postManage',
    roleName: 'postManage',
    redirect: '/postManage/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '岗位管理',
      icon: 'tab'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/post-manage'),
        name: 'postManage1',
        meta: {
          title: '岗位管理',
          icon: 'tab'
        }
      }
    ]
  },
  {
    path: '/menuManage',
    roleName: 'menuManage',
    redirect: '/menuManage/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '菜单管理',
      icon: 'example'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/menu-manage'),
        name: 'menu',
        meta: {
          title: '菜单管理',
          icon: 'example'
        }
      }
    ]
  },
  {
    path: '/roleManage',
    roleName: 'roleManage',
    redirect: '/roleManage/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '角色管理',
      icon: 'people'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/role'),
        name: 'roleList',
        meta: {
          title: '角色管理',
          icon: 'people'
        }
      }
    ]
  },
  {
    path: '/userManage',
    roleName: 'userManage',
    redirect: '/userManage/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '用户管理',
      icon: 'user'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/user-manage'),
        name: 'user-manage',
        meta: {
          title: '用户管理',
          icon: 'user'
        }
      }
    ]
  },
  {
    path: '/journal',
    roleName: 'journal',
    redirect: '/journal/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '日志管理',
      icon: 'lock'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/journal'),
        name: 'journal',
        meta: {
          title: '日志管理',
          icon: 'lock'
        }
      }
    ]
  },
  {
    path: '/system',
    roleName: 'system',
    redirect: '/system/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '系统配置',
      icon: 'nested'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/index'),
        name: 'system',
        meta: {
          title: '系统配置',
          icon: 'nested'
        }
      }
    ]
  },
  {
    path: '/hqPlatformManage',
    roleName: 'hqplatformmanagement',
    redirect: '/hqPlatformManage/list',
    // always: true,
    component: Layout,
    type: 'setAdmin',
    hidden: true,
    meta: {
      title: '中台应用管理',
      icon: 'clipboard'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/hqPlatformManage'),
        name: 'hqPlatformManage',
        roleName: 'hqplatformmanagement',
        meta: {
          title: '中台应用管理'
        }
      }
    ]
  },
  {
    path: '/hqapp-management',
    roleName: 'hqappmanagement',
    redirect: '/hqapp-management/list',
    // always: true,
    component: Layout,
    type: 'setAdmin',
    hidden: true,
    meta: {
      title: '赛道管理',
      icon: 'edit'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/hqapp-management'),
        name: 'hqapp-management',
        roleName: 'hqappmanagement',
        meta: {
          title: '赛道管理'
        }
      }
    ]
  },
  {
    path: '/dictionaries',
    roleName: 'dictionaries',
    redirect: '/dictionaries/list',
    name: 'dictionaries',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '数据字典',
      icon: 'excel'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/dictionaries'),
        name: 'dictionariesList',
        meta: {
          title: '数据字典',
          icon: 'excel'
        }
      }
    ]
  },

  // {
  //   path: '/iframe',
  //   component: Layout,
  //   redirect: '/iframe/list',
  //   roleName: 'iframe',
  //   hidden: true,
  //   always: true,
  //   type: 'setAdmin',
  //   meta: {
  //     title: '嵌套页面',
  //     icon: 'international'
  //   },
  //   children: [
  //     {
  //       path: 'list',
  //       hidden: false,
  //       component: () => import('@/views/iframe'),
  //       name: 'menuManage',
  //       meta: {
  //         title: '嵌套页面',
  //         icon: 'international'
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/demoMenu',
  //   roleName: 'demoMenu',
  //   redirect: '/demoMenu',
  //   always: true,
  //   component: Layout,
  //   hidden: true,
  //   type: 'setAdmin',
  //   name: 'demoMenu',
  //   meta: {
  //     title: '演示菜单',
  //     icon: 'icon'
  //   },
  //   children: [
  //     {
  //       path: 'demoMenu/listLayou',
  //       component: () => import('@/views/demo-menu/listLayou'),
  //       name: 'listLayou',
  //       meta: { title: '列表布局' }
  //     },
  //     {
  //       path: 'demoMenu/formTable',
  //       component: () => import('@/views/demo-menu/formTable'),
  //       name: 'formTable',
  //       meta: { title: '表单选择' }
  //     }
  //   ]
  // },

  // {
  //   path: '/newManage',
  //   roleName: 'newManage',
  //   component: Layout,
  //   hidden: true,
  //   type: 'webManagement',
  //   redirect: '/newManage/list',
  //   name: 'newManage',
  //   meta: {
  //     title: '新闻公告管理',
  //     icon: 'peoples'
  //   },
  //   children: [
  //     {
  //       path: '/list',
  //       component: () => import('@/views/new-manage/list.vue'),
  //       name: 'newlistLayou',
  //       roleName: 'newList',
  //       meta: { title: '新闻/公告列表' }
  //     },
  //     {
  //       path: '/recovery',
  //       component: () => import('@/views/new-manage/recovery.vue'),
  //       name: 'recovery',
  //       roleName: 'newRecycle',
  //       meta: { title: '新闻公告回收站' }
  //     },
  //     {
  //       path: '/createNew',
  //       component: () => import('@/views/new-manage/createNew.vue'),
  //       name: 'createNew',
  //       roleName: 'newSave',
  //       meta: { title: '创建新闻公告' }
  //     },
  //     {
  //       path: '/manage',
  //       component: () => import('@/views/new-manage/manage.vue'),
  //       name: 'manage',
  //       roleName: 'newTypeManange',
  //       meta: { title: '新闻公告类型管理' }
  //     },
  //     {
  //       path: '/cunsult',
  //       component: () => import('@/views/new-manage/cunsult.vue'),
  //       name: 'cunsult',
  //       hidden: true,
  //       always: true,
  //       meta: { title: '查阅情况' }
  //     },
  //     {
  //       path: '/detail',
  //       component: () => import('@/views/new-manage/detail.vue'),
  //       name: 'detail',
  //       hidden: true,
  //       always: true,
  //       meta: { title: '新闻公告详情' }
  //     }
  //   ]
  // },
  {
    path: '/userinfo',
    roleName: 'userinfo',
    redirect: '/userinfo/list',
    component: Layout,
    hidden: true,
    type: 'setAdmin',
    meta: {
      title: '个人信息',
      icon: 'eye-open'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/userinfo'),
        name: 'userinfo',
        meta: {
          title: '个人信息',
          icon: 'eye-open'
        }
      }
    ]
  },
  { path: '*', redirect: '/login', hidden: true }
]
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  mode: 'history',
  // routes: [...asyncRoutes, ...constantRoutes]
  routes: constantRoutes
})

const router = createRouter()
// router.beforeEach((to, from, next) => {
//   if (~~localStorage.reloadStatus === 0 && to.name === 'home') {
//     if (localStorage.userRole) {
//       // this.$store.dispatch('permission/generateRoutes', JSON.parse(localStorage.userRole))
//     }
//     // window.location.reload()
//     // localStorage.reloadStatus = 1
//   }
//   next()
// })
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export default router
