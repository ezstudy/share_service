import Qs from 'querystring'

export default {
  url: '',
  // baseURL: process.env.INTERFACE_PATH,
  // baseURL: 'http://10.0.98.227:6060/', //服务器
  // baseURL: 'http://10.0.87.138:6060/', // 业兴
  // baseURL: 'http://10.0.87.146:6060/', //健强
  baseURL: '/',
  // baseURL: (process.env.NODE_ENV === 'development') ? 'http://10.0.87.138:20001/' : 'http://10.0.98.227:6060/',
  method: 'GET',
  transformRequest: [function(data) {
    if (data) {
      data.CustData = JSON.stringify(data.CustData)
      data = Qs.stringify(data)
    }
    return data
  }],
  transformResponse: [function(data) {
    return data
  }],
  headers: {
    'Content-Type': 'application/json'
  },
  paramsSerializer: function(params) {
    return Qs.stringify(params)
  },
  timeout: 30000, // 超时时间
  withCredentials: false, // default
  responseType: 'json', // default
  onUploadProgress: function(progressEvent) {
    // Do whatever you want with the native progress event
  },
  onDownloadProgress: function(progressEvent) {
    // Do whatever you want with the native progress event
  },
  maxContentLength: 2000,
  validateStatus: function(status) {
    return status >= 200 && status < 300 // default
  },
  maxRedirects: 5 // default
}
