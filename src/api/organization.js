import request from '@/utils/request'

export function fetchTree(query) {
  return request({
    url: '/sysOrg/findOne',
    method: 'get',
    params: query
  })
}
