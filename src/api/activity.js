import request from '@/utils/activity-request'

export function getActivityList(query) {
  return request({
    url: '/tpActivity/list' + query,
    method: 'get'
  })
}

export function findActivity(query) {
  return request({
    url: '/tpActivity/info',
    method: 'get',
    params: query
  })
}

export function getAppCode(query) {
  return request({
    url: '/hqApp/list/1/10000',
    method: 'get'
  })
}

export function addActivity(query) {
  return request({
    url: '/tpActivity/save',
    method: 'post',
    data: query
  })
}

export function reviseActivity(query) {
  return request({
    url: '/tpActivity/update',
    method: 'put',
    data: query
  })
}

export function setActivityDetails(query, params) {
  return request({
    url: '/tpActivity/import/' + params,
    method: 'post',
    data: query,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function getActivityDetail(params, query) {
  return request({
    url: '/tpSubject/listPage',
    method: 'post',
    data: query,
    params
  })
}

export function getVoteDetails(query, params) {
  return request({
    url: '/tpRecord/list/' + query,
    method: 'get',
    params
  })
}

export function reviseVoteStatus(query) {
  return request({
    url: '/tpRecord/update',
    method: 'put',
    data: query
  })
}

export function downloadActDetails(query) {
  return request({
    url: '/tpSubject/export',
    method: 'put',
    data: query
  })
}

export function downloadActDetailsMore(query) {
  return request({
    url: '/tpSubject/exportMore',
    method: 'put',
    data: query
  })
}

export function downloadVoteDetails(query) {
  return request({
    url: '/tpRecord/export',
    method: 'put',
    data: query
  })
}

