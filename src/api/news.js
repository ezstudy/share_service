import request from '@/utils/request'

export function fetchNewsList(query) {
  return request({
    url: '/sysInfo/listPage',
    method: 'post',
    params: query
  })
}

export function fetchTypeList(query) {
  return request({
    url: '/sysInfoType/list',
    method: 'get',
    params: query
  })
}
