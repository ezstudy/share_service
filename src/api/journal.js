import request from '@/utils/request'

export function getJournalList(query) {
  return request({
    url: 'journal/list',
    method: 'get',
    params: query
  })
}

export function fetchJournal(id) {
  return request({
    url: 'journal/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: 'journal/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: 'journal/create',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: 'journal/update',
    method: 'post',
    data
  })
}
