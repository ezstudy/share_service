import request from '@/utils/videoRequest'

export function queryAllCategory(query) {
  return request({
    url: '/category/AllCate',
    method: 'get'
  })
}

export function queryCategory(query) {
  return request({
    url: '/category' + query,
    method: 'get'
  })
}

export function updateCategory(query) {
  return request({
    url: '/category' + query + '/' + JSON.parse(localStorage.userInfo).userName,
    method: 'put'
  })
}
export function createCategory(query) {
  return request({
    url: '/category' + query + '/' + JSON.parse(localStorage.userInfo).userName,
    method: 'POST'
  })
}
export function deleteCategory(query) {
  return request({
    url: '/category' + query + '/' + JSON.parse(localStorage.userInfo).userName,
    method: 'delete'
  })
}

export function createUploadVideo(query) {
  return request({
    url: '/video/createUploadVideo/' + JSON.parse(localStorage.userInfo).userName,
    method: 'get',
    params: query
  })
}

export function searchMedia(query) {
  return request({
    url: '/video/searchMedia',
    method: 'get',
    params: query
  })
}

export function updateVideoInfo(query) {
  return request({
    url: '/video/updateVideoInfo/' + JSON.parse(localStorage.userInfo).userName,
    method: 'put',
    params: query
  })
}

export function deleteVideo(query) {
  return request({
    url: '/video/deleteVideo' + query + '/' + JSON.parse(localStorage.userInfo).userName,
    method: 'delete'
  })
}

export function getPlayAuth(query) {
  return request({
    url: '/video/getPlayAuth/' + query,
    method: 'get'
  })
}

export function refreshUploadVideo(query) {
  return request({
    url: '/video/refreshUploadVideo/' + query,
    method: 'get'
  })
}

export function getVideoInfo(query) {
  return request({
    url: '/video/getVideoInfo/' + query,
    method: 'get'
  })
}
