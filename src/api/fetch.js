/* eslint-disable */
/**
 * @author jie
 *  data 文件夹下面放不同模块的接口
 *  method: 'Get' 'Delete' 'Post' 'Put'
 *  value: true  接口有value时 参数以键值对的值拼接到 链接后面
 *  headerToken 是否使用token 目前所有接口都默认使用
 *  post请求如果需要一部分参数带头部一部分参数body需要在请求的时候把参数独立放到 bodyParam参数里面处理
 * demo
    param = {
      id: 1,
      bodyParam: {
        key: 1
      }
    }
    this.$fetch('menuManage_getListPage', param)
 */

import Axios from 'axios'
import apiName from '../data'
import Future from './future.js'
import NProgress from 'nprogress'
import headersConfig from '../config/axios.config'
import { removeToken } from '@/utils/auth'
import {
  Message
} from 'element-ui'
// 数据请求之前
Axios.interceptors.request.use((config) => {
  NProgress.start()
  config.headers = headersConfig.headers;
  return config
}, error => {
  promise.reject(error)
})
// 提交成功并返回数据时
Axios.interceptors.response.use(response => {
  NProgress.done()
    switch (response.status) {
      case 200:
        if (response.data.code === 1131 || response.data.code === 1130 || response.data.code === 1132) {
          Message.error(response.data.msg)
          localStorage.removeItem(localStorage.oa_token)
          removeToken()
          location.href= '/login'
          return false;
        }else if(response.data.code) {
          if(response.data.code !== 200){
            Message.error(response.data.msg)
            return false
          }
        }
        return Promise.resolve(response.data)
      default:
        return Promise.resolve(response.data)
    }
  },
  error => {
    if (error.response.status) {
      switch (error.response.status) {
          // 无权限
          case 401:
              Message.error(error.response.data.msg)
              break;

          // 404请求不存在
          case 404:
              Message.error('网络请求不存在');
              break;

          // 其他错误，直接抛出错误提示
          default:
              Message.error(error.response.data.message?error.response.data.message:'网络异常，请联系管理员');
      }
      return Promise.reject(error.response);
    }
  })

class Ajax {
  constructor() {
    this.F = new Future();
  }
  ResponseHandle(res) {
    switch (res.data.Error) {
      case 1:
        this.F.reject(res.data.Msg);
        break;
      case 2:

        break;
      default:
        this.F.resolve(res.data.Data);
        break;
    }
  }
  Post(url, data, param) {
    // oss图片上传
    if (param.interFaceType === 'all') {
      const formData = new FormData()
      // 传过去oss中的key 需要把目录dir和文件名key拼接起来带过去
      formData.append('key', data.key)
      // policy
      formData.append('policy', data.policy)
      // accessid
      formData.append('OSSAccessKeyId', data.accessid)
      // 让oss返回服务器状态为200 默认是204
      formData.append('success_action_status', '200')
      // 签名 signature
      formData.append('signature', data.signature)
      // callback
      formData.append('callback', data.callback)
      formData.append('url', data.url)
      // 附件对象
      formData.append('file', data.imgFile)
      url = fileNameFromHeader(data.url)
      data = formData
    }
    // if(param.postType)
    if (param.value) {
      // data = JSON.parse(data);
      if(data.bodyParam) {
        const bodyParam = JSON.parse(JSON.stringify(data.bodyParam));
        delete data.bodyParam
        url = objectFormatJoinUrl(data, url)
        data = bodyParam
      }else {
        url = objectFormatJoinUrl(data, url)
        data = null
      }
    }
    return Axios.post(url, data)
  }
  Delete(url, data, param) {
    if(!param.value) {
        url = objectFormatJoinUrl(data, url)
    }
    else{
      url = objectFormatKeysJoinUrl(data, url)
    }

    return Axios.delete(url, data)
  }
  Put(url, data, param) {
    if (param.postType) {
      // data = JSON.parse(data);
      url = objectFormatJoinUrl(data, url)
      data = null
    }
    if(param.value){
      url = objectFormatKeysJoinUrl(data, url)
    }
    return Axios.put(url, data)
  }
  Get(url, data, param) {
    if (param.down) {
      url = data.url
      downloadFile(fileNameFromHeader(url, 1))
      return this.F.resolve(true)
    } else if(param.value){
      url = objectFormatKeysJoinUrl(data, url)
    }else {
      url = objectFormatJoinUrl(data, url)
    }
    return Axios.get(url, data)
  }
}


//对象转键值拼接url
function objectFormatKeysJoinUrl(data, url) {
    let arr = Object.keys(data);
    return url += '/' + data[arr[0]]
}
//对象拼接url
function objectFormatJoinUrl(data, url) {
  let index = 0
  for (const i in data) {
    if(data[i]){
      if (index === 0) {
        url += '?' + i + '=' + data[i]
        index++
      } else {
        url += '&' + i + '=' + data[i]
      }
    }
  }
  return url
}
// 下载流文件
function downloadFile(fileName, content) {
  var aLink = document.createElement('a')
  var blob = new Blob([content])
  var evt = document.createEvent('MouseEvents')
  evt.initEvent('click', true, true)
  if (fileName) {
    aLink.download = fileName
  }
  aLink.target = '_blank'
  aLink.href = URL.createObjectURL(blob)
  aLink.dispatchEvent(evt)
}

// oss url截取
function fileNameFromHeader(disposition, type) {
  // return disposition.split(".com")[0]+'.com';
  if (disposition) {
    const index = disposition.lastIndexOf('\/')
    return (type === 1) ? disposition.substring(index + 1, disposition.length) : disposition.split('.com')[0] + '.com'
  }
  return 'undefine_file'
}

function Fetch(name, data = {}) {
  // console.log(Boolean(apiName[name]),999999)
  if (Boolean(apiName[name].headers)) {
    for (let keys in apiName[name].headers) {
      headersConfig.headers[keys] = apiName[name].headers[keys]
    }
  }
  if(apiName[name].headerToken) {
    headersConfig.headers['token'] = localStorage.oa_token
  }

  const AjaxModule = new Ajax();
  return AjaxModule[apiName[name].method](headersConfig.baseURL + apiName[name].url, data, apiName[name])
}

// function getRealJsonData(baseStr) {
//   if (!baseStr || typeof baseStr !== 'string') return
//   var jsonData = null
//   try {
//     jsonData = JSON.parse(baseStr)
//   } catch (err) {
//     return null
//   }
//   var needReplaceStrs = []
//   loopFindArrOrObj(jsonData, needReplaceStrs)
//   needReplaceStrs.forEach(function (replaceInfo) {
//     var matchArr = baseStr.match(eval('/"' + replaceInfo.key + '":[0-9]{15,}/'))
//     if (matchArr) {
//       var str = matchArr[0]
//       var replaceStr = str.replace('"' + replaceInfo.key + '":', '"' + replaceInfo.key + '":"')
//       replaceStr += '"'
//       baseStr = baseStr.replace(str, replaceStr)
//     }
//   })
//   var returnJson = null
//   try {
//     returnJson = JSON.parse(baseStr)
//   } catch (err) {
//     return null
//   }
//   return returnJson
// }

// // 遍历对象类型的
// function getNeedRpStrByObj(obj, needReplaceStrs) {
//   for (var key in obj) {
//     var value = obj[key]
//     if (typeof value === 'number' && value > 9007199254740992) {
//       needReplaceStrs.push({
//         key: key
//       })
//     }
//     loopFindArrOrObj(value, needReplaceStrs)
//   }
// }

// // 遍历数组类型的
// function getNeedRpStrByArr(arr, needReplaceStrs) {
//   for (var i = 0; i < arr.length; i++) {
//     var value = arr[i]
//     loopFindArrOrObj(value, needReplaceStrs)
//   }
// }

// // 递归遍历
// function loopFindArrOrObj(value, needRpStrArr) {
//   var valueTypeof = Object.prototype.toString.call(value)
//   if (valueTypeof === '[object Object]') {
//     needRpStrArr.concat(getNeedRpStrByObj(value, needRpStrArr))
//   }
//   if (valueTypeof === '[object Array]') {
//     needRpStrArr.concat(getNeedRpStrByArr(value, needRpStrArr))
//   }
// }
export default Fetch
