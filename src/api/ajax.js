import apiName from '../data'
import { Message } from 'element-ui'
import { removeToken } from '@/utils/auth'
const ajaxMethod = ['Get', 'Post', 'Put', 'Delete']
const api = {}
// const ajaxPath = (process.env.NODE_ENV === 'development') ? process.env.INTERFACE_PATH: process.env.PRO_INTERFACE_PATH
// const ajaxPath = `http://10.0.98.227:6060/`
// const ajaxPath = `http://10.0.87.127:6060/`
// const ajaxPath = `http://10.0.87.146:6060/`
const ajaxPath = (process.env.NODE_ENV === 'development') ? `http://10.0.98.227:6060/` : `http://10.0.98.227:6060/`
ajaxMethod.forEach((method) => {
  // 超时时间
  const timeEnd = 60
  let startTime = null
  api[method] = function(uri, data = {}, success, failed) {
    startTime = new Date()
    if (apiName[uri].interFaceType === 'all') {
      // data = qs.stringify(data);
      const formData = new FormData()
      // 传过去oss中的key 需要把目录dir和文件名key拼接起来带过去
      formData.append('key', data.key)
      // policy
      formData.append('policy', data.policy)
      // accessid
      formData.append('OSSAccessKeyId', data.accessid)
      // 让oss返回服务器状态为200 默认是204
      formData.append('success_action_status', '200')
      // 签名 signature
      formData.append('signature', data.signature)
      // callback
      formData.append('callback', data.callback)
      formData.append('url', data.url)
      // 附件对象
      formData.append('file', data.imgFile)
      data = formData
    } else {
      data = JSON.stringify(data)
    }
    let url = null
    var xhr = null
    if (apiName[uri].interFaceType === 'api') {
      url = ajaxPath + apiName[uri].url
    } else if (apiName[uri].interFaceType === 'all') {
      url = fileNameFromHeader(data.get('url'), 2)
    }
    if (window.XMLHttpRequest) {
      xhr = new XMLHttpRequest()
    } else {
    /* eslint-disable */
      xhr = new ActiveXObject('Microsoft.XMLHTTP')
    }

    var type = method.toUpperCase()
    var random = Math.random()

    if (type === 'GET' || type === 'DELETE') {
      if (data) {
        data = JSON.parse(data)
        if (apiName[uri].down) {
          url = data.url
          xhr.responseType = 'blob'
          xhr.onload = function() {
            if (this.status === 200) {
              downloadFile(fileNameFromHeader(xhr.responseURL, 1), xhr.response)
            }
          }
        } else {
          let index = 0
          if(!apiName[uri].value) {
            for (const i in data) {
              if (index === 0) {
                url += '?' + i + '=' + data[i]
                index++
              } else {
                url += '&' + i + '=' + data[i]
              }
            }
          }else {
            let arr = Object.keys(data);
            url += '/' + data[arr[0]]
          }

        }
        xhr.open(type, url, true)
      } else {
        xhr.open(type, url + '?t=' + random, true)
      }
      debugger
      if (apiName[uri].headers) {
        for (const item of apiName[uri].headers) {
          xhr.setRequestHeader(item.key, item.value)
        }
      }
      if (apiName[uri].headerToken) {
        xhr.setRequestHeader("token", localStorage.oa_token)
      }
      xhr.send()
    } else if (type === 'POST' || type === 'PUT') {
      if (apiName[uri].postType) {
        data = JSON.parse(data)
        let index = 0
        for (const i in data) {
          if (index === 0) {
            url += '?' + i + '=' + data[i]
            index++
          } else {
            url += '&' + i + '=' + data[i]
          }
        }
        data = null
      }
      xhr.open(type, url, true)
      debugger
      if (apiName[uri].headers) {
        for(let item of apiName[uri].headers) {
            xhr.setRequestHeader(Object.keys(item)[0], Object.values(item)[0]);
        }
      }
      if (apiName[uri].headerToken) {
        xhr.setRequestHeader("token", localStorage.oa_token)
      }
      xhr.send(data)
    }

    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        const timeOut = (new Date() - startTime) / 1000
        if (xhr.status === 200 && timeOut <= timeEnd) {
          if (apiName[uri].down) {
            success(xhr.response)
          } else {
            const res = JSON.parse(xhr.responseText)
            if (res.code && res.code === 200 || res.code === 0) {
              success(res)
            } else if (res.data === null) {
              setTimeout(() => {
                localStorage.ajaxTimeLock = true
              }, 5000)
              if (localStorage.ajaxTimeLock) {
                Message.error(res.message)
                localStorage.ajaxTimeLock = false
              }
            }else {
              Message.error(res.message || res.msg)
              //登陆失效
              if(res.code === 1131 || res.code === 1130 || res.code === 1132) {
                localStorage.clear("oa_token")
                removeToken()
                window.location.reload()
              }
            }
          }
        } else {
          try {
            const res = JSON.parse(xhr.responseText)
            Message.error(res.message || res.msg)
            //登陆失效
            if(res.code === 1131 || res.code === 1130 || res.code === 1132) {
              removeToken()
              localStorage.clear("oa_token")
              window.location.reload()
              // window.location.href = window.location.href.split("#/")[0] + '#/login'
            }
            setTimeout(() => {
              localStorage.ajaxTimeLock = true
            }, 5000)
            if (localStorage.ajaxTimeLock) {
              localStorage.ajaxTimeLock = false
              if (timeOut > timeEnd) {
                Message.error('请求超时。。。')
              } else {
                Message.error('请求失败。。。')
              }
            }
          } catch (error) {
            // removeToken()
            //   localStorage.clear("oa_token")
            //   window.location.reload()
          }
          if (failed) {
            failed(xhr.status)
          }
        }
      }
    }
  }
})

function fileNameFromHeader(disposition, type) {
  if (disposition) {
    const index = disposition.lastIndexOf('\/')
    return (type === 1) ? disposition.substring(index + 1, disposition.length) : disposition.split('.com')[0] + '.com'
  }
  return 'undefine_file'
}

function downloadFile(fileName, content) {
  var aLink = document.createElement('a')
  var blob = new Blob([content])
  var evt = document.createEvent('MouseEvents')
  evt.initEvent('click', true, true)
  if (fileName) {
    aLink.download = fileName
  }
  aLink.target = '_blank'
  aLink.href = URL.createObjectURL(blob)
  aLink.dispatchEvent(evt)
}

export default api
