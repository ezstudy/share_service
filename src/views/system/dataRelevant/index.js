import Ajax from '@/api/ajax'
class DataRelevant {
  getpagingData(obj, cb) {
    // 分页数据
    const submitData = obj
    Ajax.Get('getSystemListPage', submitData, res => {
      typeof cb === 'function' && cb(res)
    })
  }
  addSystemItem(row, cb) {
    // 增加每一条配置 addSystemItem
    const submitData = {
      code: row.code,
      // configId: row.configId,
      // createId: row.createId,
      createTime: row.currentDate,
      infoId: row.memo,
      memo: row.memo,
      name: row.name,
      // status: row.status,
      type: row.type
      // updateId: row.updateId,
      // updateTime: row.newUpdateTime,
      // value: row.value
    }
    Ajax.Post('addSystemItem', submitData, (res) => {
      typeof cb === 'function' && cb(res)
    })
  }
  updateSystemItem(row, cb) {
    // 编辑更新每一条配置
    const submitData = {
      code: row.code,
      createId: row.createId,
      createTime: row.currentDate,
      infoId: row.memo,
      id: row.id || row.uuid,
      memo: row.memo,
      name: row.name,
      status: row.status,
      type: row.type,
      updateId: row.updateId,
      updateTime: row.newUpdateTime,
      value: row.value
    }
    Ajax.Put('updateSystemItem', submitData, (res) => {
      typeof cb === 'function' && cb(res)
    })
  }
  delSystemItem(item, cb) {
    // 删除每一条配置
    const row = item.row
    const submitData = {
      id: row.id
    }
    Ajax.Delete('delSystemItem', submitData, res => {
      typeof cb === 'function' && cb(res)
    })
  }
}
export default new DataRelevant()
