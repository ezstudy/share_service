import { asyncRoutes, constantRoutes } from '@/router'
import Vue from 'vue'
import Fetch from '@/api/fetch'
/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []
  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

const state = {
  routes: [],
  addRoutes: [],
  buttonPermissionsList: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = JSON.parse(JSON.stringify(constantRoutes.concat(routes)))
  },
  CLEAR_ROUTES: (state) => {
    state.routes = []
    Vue.set(state.routes, [])
  },
  FILTER_ROUTES: (state, value) => {
    const arr = asyncRoutes.filter((item) => item.type === value)
    state.routes = constantRoutes.concat(arr)
  },
  BUTTON_ROUTES: (state, routes) => {
    state.buttonPermissionsList = [...routes, ...[]]
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      const arr = new Map()
      commit('BUTTON_ROUTES', roles)
      for (const item of roles) {
        item.name && arr.set(item.name.trim(), { title: item.title, icon: item.icon, menuId: item.menuId })
      }
      let accessedRoutes = []
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        const json = {}
        accessedRoutes = asyncRoutes.filter((item) => {
          if (arr.get(item.roleName) || item.always) {
            if (!json[item.type]) {
              json[item.type] = 1
            }
            if (arr.get(item.roleName)) {
              // console.log(arr.get(item.roleName).title)
              // item.meta = {
              //   title: arr.get(item.roleName).title,
              //   icon: arr.get(item.roleName).icon
              // }
              // console.log(item.meta)
              // const obj = JSON.parse(JSON.stringify(item))
              // if(obj.meta){
              //   item.meta = {
              //     title: arr.get(item.roleName).title,
              //     icon: obj.meta.icon
              //   }
              // }
              if (item.children && item.children.length) {
                item.menuId = arr.get(item.roleName).menuId
                if (item.children.length > 1) {
                  item.children = item.children.filter((childrenItem) => {
                    if (arr.get(childrenItem.roleName)) {
                      childrenItem.menuId = arr.get(childrenItem.roleName).menuId
                    }
                    return arr.get(childrenItem.roleName) || childrenItem.always
                  })
                } else {
                  item.children[0].meta = {
                    title: arr.get(item.roleName).title,
                    icon: arr.get(item.roleName).icon
                  }
                }
              }
            }
            item.hidden = false
            return true
          } else {
            return false
          }
        })
        console.log(accessedRoutes, 'accessedRoutes')
        localStorage.json = JSON.stringify(json)
      }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  },
  async getButtonPower({ state }, name) {
    const list = state.buttonPermissionsList
    const menuList = list.filter((item) => {
      return item.name === name
    })
    const res = await Fetch('user_userPermByMenuId', { menuId: menuList[0].menuId })
    const json = {}
    for (const item of res.result) {
      const itemList = item.split(':')
      json[(itemList[itemList.length - 1])] = true
    }
    return json
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
