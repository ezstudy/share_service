import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
// import Ajax from '@/api/ajax'
import Fetch from '@/api/fetch'
import i18n from './lang' // internationalization
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import AnimatedVue from 'animated-vue'
import animated from 'animate.css'
Vue.use(AnimatedVue)
Vue.use(animated)
// add
// import hqTool from './assets/js/hqTool'
/**
 * If you don't want to use mock-server
 * you want to use mockjs for request interception
 * you can execute:
 *
 * import { mockXHR } from '../mock'
 * mockXHR()
 */
// console.log(router)
Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})

// register global utility filters
// Object.keys(filters).forEach(key => {
//   Vue.filter(key, filters[key])
// })
// function sum(m, n) {
//   return Math.floor(Math.random() * (m - n) + n)
// }
// const phoneArray = [
//   '15170073501',
//   '18643018988',
//   '17875514476',
//   '13226258217',
//   '18680222164',
//   '13570309450',
//   '13719040151',
//   '18320293110',
//   '13926187857',
//   '15013759630',
//   '13007382642',
//   '13530116189',
//   '13612341234',
//   '18814128605',
//   '15323307827',
//   '13178883409',
//   '13610148485',
//   '13502468549',
//   '13631150627',
//   '18826406959',
//   '13727508762',
//   '13580446908',
//   '18825048081',
//   '18826417127',
//   '15773974799',
//   '17600628788',
//   '13113358521',
//   '13719457581',
//   '15570676791',
//   '13513358521',
//   '13413358521',
//   '13325566444'
// ]
// 登录
// localStorage.oa_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIzIiwianR1Ijoi6L-Q6JCl57uP55CGIiwiZXhwIjoxODY4MjQxMTc1LCJpYXQiOjE1NTcyMDExNzUsImp0aSI6ImZkNThkMzBjLTNkMDctNDliZi1iYTQ0LTVhOGQ3NjUwOGRlNyJ9.YCD0_IiviqIrPKcKxKcNrx3JuogktmhDpnB3F6NfFKw'
// !localStorage.oa_token && Ajax.Post('login', { phone: phoneArray[sum(1, phoneArray.length - 1)], password: 123456 }, (res) => {
// !localStorage.oa_token && Ajax.Post('login', { phone: 'admin', password: 123456 }, (res) => {
//   if (res.code === 200) {
//     localStorage.oa_token = res.result.token
//   }
// })
// Vue.prototype.$hqTool = hqTool
// Vue.prototype.$ajax = Ajax
Vue.prototype.$fetch = Fetch
Vue.config.productionTip = false
new Vue({
  el: '#app',
  store,
  router,
  i18n,
  render: h => h(App)
})
