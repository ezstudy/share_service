/**
 * 岗位管理相关接口
 */

export default {
  // 岗位用户列表
  post_getListPostUser: {
    url: 'oa-api/sysPost/listPostUser',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 岗位用户列表
  post_getListPostByOrgId: {
    url: 'oa-api/sysCommon/sysPost/listPostByOrgId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 用户管理范围列表
  post_getuserRangeInfo: {
    url: 'oa-api/sysPost/userRangeInfo',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  // 岗位管理范围列表
  post_getsysPost: {
    url: 'oa-api/sysPost',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  // 岗位保存
  post_sysPostSave: {
    url: 'oa-api/sysPost/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 岗位更新
  post_sysPostUpdate: {
    url: 'oa-api/sysPost/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 添加用户
  post_saveUserPostList: {
    url: 'oa-api/sysPost/saveUserPostList',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 保存岗位管理范围
  post_savePostRange: {
    url: 'oa-api/sysPost/savePostRange',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 保存用户管理范围
  post_saveUserRange: {
    url: 'oa-api/sysPost/saveUserRange',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 修改组织机构岗位标记
  post_updatePostSign: {
    url: 'oa-api/sysOrg/updatePostSign',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // // 更新用户管理范围
  // post_updateUserRange: {
  //   url: 'oa-api/sysPost/updateUserRange',
  //   interFaceType: 'api',
  //   headerToken: true,
  //   method: 'Put'
  // },
  // // 更新岗位管理范围
  // post_update: {
  //   url: 'oa-api/sysPost/update',
  //   interFaceType: 'api',
  //   headerToken: true,
  //   method: 'Put'
  // },
  // 删除岗位
  post_delete: {
    url: 'oa-api/sysPost/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  }

}

