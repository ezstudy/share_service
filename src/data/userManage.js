/**
 * 用户管理相关接口
 */

export default {
  // 获取列表数据
  user_getListPage: {
    url: 'oa-api/user/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 同步用户数据
  user_syncUser: {
    url: 'oa-api/sync/syncUser',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  },
  // 修改个人信息
  user_update: {
    url: 'oa-api/user/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 删除个人信息
  user_delete: {
    url: 'oa-api/user/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 保存个人信息
  user_save: {
    url: 'oa-api/user/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 岗位用户列表
  user_getListPostUser: {
    url: 'oa-api/sysPost/listPostUser',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  journal_getListPost: {
    url: 'oa-api/log/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  //  获取组织架构相关用户
  user_getUserInfoByOrgId: {
    url: 'oa-api/sysCommon/userNameListByOrgId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 更新用户信息
  user_getInfo: {
    url: 'oa-api/user/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 获取当前角色列表、
  user_getRole: {
    url: 'oa-api/sysUserInfo/rolelist',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 更新个人头像
  user_updateAvatar: {
    url: 'oa-api/sysUserInfo/img/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 重置密码
  user_resetPassword: {
    url: 'oa-api/user/resetPassword',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  }
}

