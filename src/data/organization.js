/**
 * 组织架构相关接口
 */

export default {
  //  获取组织架构树状列表
  organization_getTree: {
    url: 'oa-api/sysCommon/sysOrg/findOne',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 获取数据字典项
  get_dict_data: {
    url: 'oa-api/dict/find',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },

  // 获取用户信息
  get_user_info: {
    url: 'oa-api/user/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },

  // 新增组织架构
  organ_add: {
    url: 'oa-api/sysOrg/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },

  // 修改组织架构
  organ_edit: {
    url: 'oa-api/sysOrg/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },

  // 删除组织架构
  organ_delete: {
    url: 'oa-api/sysOrg/delete',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  },

  // 根据id查询列表
  organ_getList: {
    url: 'oa-api/sysOrg/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post',
    value: true
  },

  // 搜索树状列表
  organ_search: {
    url: 'oa-api/sysOrg/treeList',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 获取组织机构信息
  organ_info: {
    url: 'oa-api/sysOrg',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  }
}
