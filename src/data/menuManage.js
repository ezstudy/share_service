/**
 * 菜单相关接口
 */

export default {
  //  获取tree菜单
  menuManage_getTree: {
    url: 'oa-api/menu/select',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  //  获取tree菜单带权限
  menuManage_getPowerTree: {
    url: 'oa-api/menu/tree',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  //  获取list菜单
  menuManage_getListPage: {
    url: 'oa-api/menu/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 添加菜单
  menuManage_add: {
    url: 'oa-api/menu/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 更新菜单
  menuManage_update: {
    url: 'oa-api/menu/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 禁用菜单
  menuManage_forbidden: {
    url: 'oa-api/menu/forbidden',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 删除菜单
  menuManage_delete: {
    url: 'oa-api/menu/delete',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  },
  // 菜单信息
  menuManage_getMenu: {
    url: 'oa-api/menu',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  }

}

