/**
 * 主页相关接口
 */

export default {
  //  获取新闻公告
  home_getList: {
    url: 'oa-api/sysInfo/listPage',
    interFaceType: 'api',
    headerToken: true,
    // isOpen: false,
    headers: [{
      'Content-Type': 'application/json'
    }]
  },
  // 删除日志 支持批量
  journal_delete: {
    url: 'oa-api/log/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    // isOpen: false,
    headers: [{
      'Content-Type': 'application/json'
    }]
  },

  // 获取个人信息
  home_getUser: {
    url: 'oa-api/sysUserInfo/info',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 修改密码
  home_updateUser: {
    url: 'oa-api/sysUserInfo/password/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  }
}

