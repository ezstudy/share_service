/**
 * 新闻相关接口
 */

export default {
  //  获取新闻列表
  news_getList: {
    url: 'oa-api/sysInfo/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post',
    value: true
  },
  // 获取类型下拉列表
  news_getTypeList: {
    url: 'oa-api/sysInfoType/list',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 保存系统信息
  news_save: {
    url: 'oa-api/sysInfo/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },

  // 修改系统信息
  news_update: {
    url: 'oa-api/sysInfo/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },

  // 恢复系统信息
  news_recover: {
    url: 'oa-api/sysInfo/updateRecycle',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  },
  // 删除回收站系统信息
  news_deleteRecycle: {
    url: 'oa-api/sysInfo/deleteRecycle',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  },

  // 修改新闻类型
  news_updateType: {
    url: 'oa-api/sysInfoType/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },

  // 新增新闻类型
  news_addType: {
    url: 'oa-api/sysInfoType/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },

  // 删除新闻类型
  news_deleteType: {
    url: 'oa-api/sysInfoType/delete',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete'
  },

  // 置顶系统信息
  news_stick: {
    url: 'oa-api/sysInfo/stick',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },

  // 批准系统信息
  news_ratify: {
    url: 'oa-api/sysInfo/ratify',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },

  // 删除系统信息
  news_deleteList: {
    url: 'oa-api/sysInfo/deletelist',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  },

  // 查阅情况
  news_read: {
    url: 'oa-api/sysInfo/read',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 拒绝
  news_refuse: {
    url: 'oa-api/sysInfo/refuse',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 查看详情
  news_detail: {
    url: 'oa-api/sysInfo',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },

  // 撤销
  news_revoke: {
    url: 'oa-api/sysInfo/revoke',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 下线
  news_offline: {
    url: 'oa-api/sysInfo/offline',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 上线
  news_online: {
    url: 'oa-api/sysInfo/online',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 获取未读消息
  news_getUnread: {
    url: 'oa-api/sysInfo/unReadListPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },

  // 获取信息详情
  news_getDetail: {
    url: 'oa-api/sysInfo',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },

  // 获取信息id
  news_getInfoId: {
    url: 'oa-api/sysInfo/infoId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 保存附件信息
  news_saveInfo: {
    url: 'oa-api/sysInfoAnnex/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },

  // 获取infoId信息
  new_getInfoDetail: {
    url: 'oa-api/sysInfoAnnex/InfoId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  // 删除infoId所有附件
  news_deleteAllAppendix: {
    url: 'oa-api/sysInfoAnnex/delete',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete'
  }
}

