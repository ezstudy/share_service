export default {
  // 获取图片上传签名
  fsSignature_get: {
    url: 'oa-api/sysUserInfo/signature',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 获取图片地址
  all: {
    url: '',
    interFaceType: 'all',
    headerToken: true,
    method: 'Post'
  },
  // 下载文件
  downFile: {
    url: '',
    interFaceType: 'api',
    method: 'Get',
    down: true
  }
}
