export default {
  get_liveList: {
    url: 'live-api/videoLive/list',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  get_liveDetail: {
    url: 'live-api/videoLive/info',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  // 创建直播/videoLive/put
  put_liveDetail: {
    url: 'live-api/videoLive/put',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 更新直播信息
  post_liveDetail: {
    url: 'live-api/videoLive/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 删除直播
  delete_liveDetail: {
    url: 'live-api/videoLive/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 批量删除
  delete_liveDetailMore: {
    url: 'live-api/videoLive/batchDeleted/',
    interFaceType: 'api',
    headerToken: true,
    // value: true,
    method: 'Delete'
  },
  // 获取老师信息
  get_liveTeacher: {
    url: 'live-api/sysUser/list/1/20',
    interFaceType: 'api',
    headerToken: true,
    // value: true,
    method: 'Get'
  },
  // 根据id获取老师信息
  get_liveTeacherById: {
    url: 'live-api/sysUser/info',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  // 查看用户观看列表数据
  get_liveAudience: {
    url: 'live-api/watchUserRecord/list',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  // 导出观众名单
  upload_live_audience: {
    url: 'live-api/watchUserRecord/exportExcel2',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  },
  // 获取下载客户端地址
  get_downloadSrc: {
    url: 'live-api/obsClientUrl/getUrl',
    interFaceType: 'api',
    headerToken: true,
    // value: true,
    method: 'Get'
  },
  // 获取直播回放列表
  get_playbackList: {
    url: 'live-api/livePlayback/playbackList',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 获取直播并发数据
  get_watchUserNum: {
    url: 'live-api/videoLive/getLiveStreamHistoryUserNum',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 删除回放
  delete_playBack: {
    url: 'live-api/livePlayback/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  }
}
