export default {
  // 获取课程数据
  get_course_list: {
    url: 'course-api/v1/courses',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 获取产品线数据
  get_product_list: {
    url: 'course-api/v1/sys/products',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 修改课程数据
  save_course: {
    url: 'course-api/v1/courses',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Put'
  },
  // 删除课程
  delete_course: {
    url: 'course-api/v1/courses',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 新增课程
  add_course: {
    url: 'course-api/v1/courses',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  },
  // 获取课程阶段数据
  get_course_stages: {
    url: 'course-api/v1/stages',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 修改课程阶段数据
  save_course_stages: {
    url: 'course-api/v1/stages',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Put'
  },
  // 删除课程阶段
  delete_course_stages: {
    url: 'course-api/v1/stages',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 新增课程阶段
  add_course_stages: {
    url: 'course-api/v1/stages',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  },
  // 获取课程课次数据
  get_course_items: {
    url: 'course-api/v1/course/items',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 修改课程课次数据
  save_course_items: {
    url: 'course-api/v1/course/items',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Put'
  },
  // 删除课程课次
  delete_course_items: {
    url: 'course-api/v1/course/items',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 新增课程课次
  add_course_items: {
    url: 'course-api/v1/course/items',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  }
}
