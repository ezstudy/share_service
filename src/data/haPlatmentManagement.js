export default {
  /* 系统配置模块相关API-----------------------------------------------------------------------*/
  //  获取分页数据
  hqappDetails: {
    url: 'oa-api/hqApp/list',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },
  hqapp_add: {
    url: 'oa-api/hqApp/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 修改每一条配置
  hqapp_update: {
    url: 'oa-api/hqApp/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // // 增加一条配置 config/save
  // system_addSystemItem: {
  //   url: 'oa-api/config/save',
  //   interFaceType: 'api',
  //   headerToken: true,
  //   method: 'Post'
  // },
  // // 删除每一项配置
  // system_delSystemItem: {
  //   url: 'oa-api/config/delete',
  //   interFaceType: 'api',
  //   headerToken: true,
  //   value: true,
  //   method: 'Delete'
  // }

  // 应用平台列表
  hqPlatform_listPage: {
    url: 'oa-api/hqPlatform/list',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },
  // 通过应用平台查看列表数据
  hqApp_listPage: {
    url: 'oa-api/hqApp/listByPlatformId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get',
    value: true
  },
  // 新增应用平台
  hqplayform_add: {
    url: 'oa-api/hqPlatform/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 修改应用平台
  hqplayform_update: {
    url: 'oa-api/hqPlatform/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  }
}
