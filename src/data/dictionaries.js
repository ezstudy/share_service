export default {
  // getDicitionariesListPage
  dic_getDicitionariesListPage: {
    url: 'oa-api/dict/dir/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },

  dic_getDicitionariesListPagePost: {
    url: 'oa-api/dict/dir/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 删除字典
  dic_deleteDicitionaries: {
    url: 'oa-api/dict/delete',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  },
  // 删除字典目录
  dic_deleteDicCatalog: {
    url: 'oa-api/dict/dir',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 字典分页
  dic_listPage: {
    url: 'oa-api/dict/listPage',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  },
  // 保存字典
  dic_saveDicitionaries: {
    url: 'oa-api/dict/save',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Post'
  },

  // 修改字典
  dic_updateDicitionaries: {
    url: 'oa-api/dict/update',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Put'
  },
  // 保存字典目录
  dic_saveDicCatalog: {
    url: 'oa-api/dict/dir/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 修改字典目录
  dic_updateDicCatalog: {
    url: 'oa-api/dict/dir/update',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Put'
  },

  /**
   * 待分类接口
  */
  getRoleslistPage: {
    url: 'oa-api/role/listPage',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    headers: [{
      'Content-Type': 'application/json'
    }]
  },
  getDicitionariesList: {
    url: 'oa-api/dict/dir/list',
    interFaceType: 'api',
    headerToken: true,
    // isOpen: false,
    headers: [{
      'Content-Type': 'application/json'
    }]
  },
  // 登陆接口
  login: {
    url: 'oa-api/login',
    interFaceType: 'api',
    // postType: true,
    // isOpen: false,
    headers: [{
      'Content-Type': 'application/json'
    }]
  },
  getDicitionariesFind: {
    url: 'oa-api/dict/find',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    // isOpen: false,
    headers: [{
      'Content-Type': 'application/json'
    }]
  }
}
