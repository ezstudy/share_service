import user from './user'
import dictionaries from './dictionaries'
import system from './system'
import journal from './journal'
import menuManage from './menuManage'
import postManage from './postManage'
import userManage from './userManage'
import organization from './organization'
import oss from './oss'
import roleManage from './roleManage'
import home from './home'
import news from './news'
import pay from './pay'
import liveManage from './liveManage'
import course from './course'
import haPlatmentManagement from './haPlatmentManagement.js'

export default {
  ...user,
  ...home,
  ...system,
  ...journal,
  ...dictionaries,
  ...menuManage,
  ...news,
  ...organization,
  ...oss,
  ...postManage,
  ...userManage,
  ...roleManage,
  ...pay,
  ...liveManage,
  ...course,
  ...haPlatmentManagement
}
