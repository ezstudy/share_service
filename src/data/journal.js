/**
 * 日志相关接口
 */

export default {
  //  获取获取日志列表
  journal_getList: {
    url: 'oa-api/log/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  journal_getListPost: {
    url: 'oa-api/log/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 删除日志 支持批量
  journal_delete: {
    url: 'oa-api/log/delete',
    interFaceType: 'api',
    headerToken: true,
    method: 'Delete',
    value: true
  }
}

