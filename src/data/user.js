
export default {
// 登陆接口
  login: {
    url: 'oa-api/login',
    interFaceType: 'api',
    // postType: true,
    // isOpen: false,
    headers: [{
      'Content-Type': 'application/json'
    }]
  },
  // 登陆接口
  user_login: {
    url: 'oa-api/login',
    interFaceType: 'api',
    method: 'Post'
  },
  // 菜单权限
  user_menuRole: {
    url: 'oa-api/userMenuName',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 菜单权限详细控制
  user_menuRolePerm: {
    url: 'oa-api/userPerm',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 菜单按钮权限
  user_userPermByMenuId: {
    url: 'oa-api/userPermByMenuId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  }
}
