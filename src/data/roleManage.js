/**
 * 角色管理管理相关接口
 */

export default {
  // 角色列表
  role_listPage: {
    url: 'oa-api/role/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 保存角色
  role_save: {
    url: 'oa-api/role/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 修改角色
  role_update: {
    url: 'oa-api/role/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 删除角色
  role_delete: {
    url: 'oa-api/role/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  },
  // 保存用户角色集合
  role_saveUserRoleList: {
    url: 'oa-api/role/saveUserRoleList',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 获取角色的权限等信息
  role_getRoleInfo: {
    url: 'oa-api/role',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Get'
  },
  // 获取角色的人员情况
  role_getUserListByRoleId: {
    url: 'oa-api/role/userListByRoleId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 公共接口获取角色的人员情况
  role_comListByRoleId: {
    url: 'oa-api/sysCommon/userListByRoleId',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  }
}

