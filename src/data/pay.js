/**
 * 支付系统相关接口
 */

export default {
  //  获取获取日志列表
  payorder_getList: {
    url: 'pay-api/payOrder/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post',
    value: true
  },
  // 多条件导出
  payorder_select_downloads: {
    url: 'pay-api/payOrder/outputPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post',
    value: true
  },
  // 批量导出
  payorder_downloads: {
    url: 'pay-api/payOrder/outputPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 平台配置列表
  pay_merch_list: {
    url: 'pay-api/merchInfo/list',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  // 平台配置列表
  pay_merch_save: {
    url: 'pay-api/merchInfo/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 平台配置列表
  pay_merch_update: {
    url: 'pay-api/merchInfo/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 平台配置列表
  pay_merch_forbidden: {
    url: 'pay-api/merchInfo/updateStatus',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 平台配置列表
  pay_merch_selectList: {
    url: 'pay-api/merchInfo/selectList',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  }
}

