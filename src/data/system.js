export default {
  /* 系统配置模块相关API-----------------------------------------------------------------------*/
  //  获取分页数据
  system_getSystemListPage: {
    url: 'oa-api/config/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Get'
  },
  system_getSystemListPagePost: {
    url: 'oa-api/config/listPage',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 修改每一条配置
  system_updateSystemItem: {
    url: 'oa-api/config/update',
    interFaceType: 'api',
    headerToken: true,
    method: 'Put'
  },
  // 增加一条配置 config/save
  system_addSystemItem: {
    url: 'oa-api/config/save',
    interFaceType: 'api',
    headerToken: true,
    method: 'Post'
  },
  // 删除每一项配置
  system_delSystemItem: {
    url: 'oa-api/config/delete',
    interFaceType: 'api',
    headerToken: true,
    value: true,
    method: 'Delete'
  }
}
