## 分支说明
v1.0 直播管理

v1.1 中台页面管理

## 目录说明
```javascript
//文件路径 /src/router/index.js
{
  path: '/hqapp-management',
  roleName: 'hqappmanagement',//找后端配置菜单权限，名字需要与roleName一致
  redirect: '/hqapp-management/list',
  always: true, //设置了这个参数等于写死这个菜单目录，会一直显示
  component: Layout,
  type: 'setAdmin',// setAdmin:系统设置  play：基础系统
  hidden: true,
  meta: {
    title: '赛道管理',
    icon: 'edit'  //在svg目录找一个
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/hqapp-management'),
      name: 'hqapp-management',
      roleName: 'hqappmanagement',
      meta: {
        title: '赛道管理'
      }
    }
  ]
},

```

## 开发

```bash
# 克隆项目
git clone https://github.com/PanJiaChen/vue-element-admin.git

# 进入项目目录
cd vue-element-admin

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:9527

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```

